# specify a suitable source image
# ADDED NODE
# AND CHANGED VERSOIN TO 16 AS
# AZURE NO LONGER SUPPORTED 12, AND
# version 14 WAS LOSING SUPPORT TOO
FROM node:16

WORKDIR /app

COPY package*.json ./

RUN npm ci

# copy the application source code files
# ADDED COPYING OF THE SOURCE FILES
COPY . .

EXPOSE 3000

# specify the command which runs the application
# ADDED COMMAND TO RUN NPM
CMD ["npm", "start"]